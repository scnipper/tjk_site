<?php

$admin = $_COOKIE['admin_session'];

$rest_json = file_get_contents("php://input");
$_POST = json_decode($rest_json, true);



if($admin != '') {

	$data = explode("|", $admin);

	$login_ = $data[0];
	$pass_ = $data[1];
	$fileConf = "../config/conf.json";
	$c = file_get_contents($fileConf);
	$config = json_decode($c,true);


	if($login_ == $config['login'] && $pass_ == $config['password']) {
		$fileSocial = "../social_list.json";
		$links = json_decode(file_get_contents($fileSocial));

		$itemsPost = $_POST["items"];

		foreach ($itemsPost as $key => $value) {
			$links[$key]->link = $value;
		}

		if(file_put_contents($fileSocial, json_encode($links)) === false) {
			echo "no save file";
		} else {
			echo "ok";
		}

	} else {
		echo " no login";
	}
} else {
	echo " no login";
}
?>