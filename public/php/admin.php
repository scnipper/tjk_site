<!DOCTYPE html>
<html>
<head>
	<title>Admin page</title>
	 <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

	<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<style type="text/css">
	.enter-login {
		width: 40%;
		margin-left: auto;
		margin-right: auto;
	}
	.title {
		text-align: center;
	}
	.content-main {
		width: 50%;
		margin-left: auto;
		margin-right: auto;
		margin-top: 20px;
	}
</style>
<script type="text/javascript">


	function checkLogin() {
		var login = document.getElementById("login").value
		var pass = document.getElementById("pass").value

		if(login === "" || pass === "") {
			var alertEmpty = document.getElementById("empty-alert")
			alertEmpty.style.display = "block"
		} else {
			document.cookie = "admin_session="+login+"|"+pass+"; max-age=43200"
			document.location.reload(true);
		}
	}
</script>
</head>
<body>


<?php

$admin = $_COOKIE['admin_session'];


if($admin == '') {
	$loginPage = file_get_contents("templ/login_in_admin.html");

	echo $loginPage;
} else {
	$data = explode("|", $admin);

	$login = $data[0];
	$pass = $data[1];
	$c = file_get_contents("../config/conf.json");
	$config = json_decode($c,true);

	$linkMedalMainPage = json_decode(file_get_contents("../link_main_page.json"));
	$tablePrices = json_decode(file_get_contents("../table_prices.json"));

    $lang = $_COOKIE['lang'];

    if($lang == ''){
        $lang = 'ru';
    }

	$medalList = json_decode(file_get_contents("../medal_list_$lang.json"));
	$socialList = json_decode(file_get_contents("../social_list.json"));


	if($login == $config['login'] && $pass == $config['password']) {
		echo "<h3>Вы вошли под логином: <b>".$login."</b></h1>";

echo <<<HTML
<script type="text/javascript">
function saveSocialLinks() {
	var allLinks = document.getElementsByClassName("social-contact")
	var newArrayLinks = []
	for(var i = 0;i<allLinks.length;i++) {
		newArrayLinks.push(allLinks[i].value)
	}

	
	var saveObj = {
		items: newArrayLinks
	};

	axios.post('/php/save_social.php',saveObj)
  	.then(function (response) {
  		if(response.data == "ok") {
  			alert("Успешное сохранение")
  		} else {
  			alert("Ошибка сохранения: "+response.data)
  		}
  	});

}
function savePriceTable() {
	var allLinks = document.getElementsByClassName("table-price")
	var newArrayLinks = []
	for(var i = 0;i<allLinks.length;i++) {
		newArrayLinks.push(allLinks[i].value)
	}

	
	var saveObj = {
		items: newArrayLinks
	};

	axios.post('/php/save_table.php',saveObj)
  	.then(function (response) {
  		if(response.data == "ok") {
  			alert("Успешное сохранение")
  		} else {
  			alert("Ошибка сохранения: "+response.data)
  		}
  	});
}
function saveSettings() {
	//const axios = require('axios');

	var email = document.getElementById("email-change").value
	var pass = document.getElementById("password-change").value
	var login = document.getElementById("login-change").value
	var you = document.getElementById("youtube-link").value

	var medal1 = document.getElementById("main-page-medal-1").value
	var medal2 = document.getElementById("main-page-medal-2").value

	

	axios.get('/php/save_settings.php?email='+email+"&pass="+pass+"&login="+login+"&medal1="+medal1+"&medal2="+medal2+"&youtube="+you)
  		.then(function (response) {
  			if(response.data === "ok") {
  				alert("Успешное сохранение")

  			} else {
  				alert("Ошибка сохранения "+response.data)
  			}
  })

}

function deleteBestPunkt(event) {
	var parent = event.parentNode.parentNode
	parent.parentNode.removeChild(parent)
}
function addMedal(el) {
	var parent = el.parentNode

	var newEl = document.createElement("div")
	newEl.setAttribute('class','card')

	newEl.innerHTML = '\
	\ <img src="/img/MedalList/medal_00.png" class="card-img-top">\
  <div class="card-body">\
<div class="form-group">\
    <label for="exampleFormControlFile1">Картинка</label>\
    <input type="file" class="form-control-file" accept="image/*" id="exampleFormControlFile1">\
          <input class="img-name" value="medal_1.png" type="hidden">\
  </div>\
      <label>Название</label>\
 <input value="" type="text" class="form-control medal-name" aria-describedby="emailHelp">\
 <label>Характеристики</label>\
<div class="input-group mb-3">\
  <input value="" type="text" class="form-control best-list" aria-describedby="button-addon2">\
  <div class="input-group-append">\
    <button class="btn btn-outline-secondary" onclick="deleteBestPunkt(this)" type="button" >Удалить</button>\
  </div>\
</div>\
<button type="button" onclick="addBestPunkt(this)" class="btn btn-success">Добавить</button>\
<button type="button" onclick="deleteBestPunkt(this)" class="btn btn-danger">Удалить значок</button>\
</div>'

  	parent.insertBefore(newEl,parent.children[parent.children.length-2])

}
function saveListMedal(el) {
	var names = document.getElementsByClassName("medal-name")
	var outJson = []
	for(var i = 0;i<names.length;i++) {
		var item = names[i]

		var bestItem = item.parentNode.getElementsByClassName("best-list")
		var arrBest = []

		for(var j = 0;j<bestItem.length;j++) {
			arrBest.push(bestItem[j].value)
		}


		var obj = {
			medal_num: "",
			desc: item.value,
			list_best: arrBest
		}
		outJson.push(obj)
	}

	var countImages = 0


	for(var j = 0;j<names.length;j++) {
		var item = names[j]
		var imgName = item.parentNode.getElementsByClassName("img-name")[0].value
		var formFile = item.parentNode.getElementsByClassName("form-control-file")[0]
		if(formFile.value !== "") {
			countImages++;
		}

	}

	for(var j = 0;j<names.length;j++) {
		var item = names[j]
		var imgName = item.parentNode.getElementsByClassName("img-name")[0].value
		var formFile = item.parentNode.getElementsByClassName("form-control-file")[0]


		let saveIndex = j
		if(formFile.value !== "") {
			var formData = new FormData();
			formData.append("medal_file", formFile.files[0]);
			axios.post('/php/upload_img.php', formData, {
    		headers: {
      			'Content-Type': 'multipart/form-data'
    		}
			}).then(function(res) {
				countImages--;
				if(res.data == "no") {
    			outJson[saveIndex].medal_num = imgName
  				} else {
    			outJson[saveIndex].medal_num = res.data
  				}
    			if( countImages == 0) {
  					completeSaveMedal(outJson)
  				}
			}).catch(function (error) {
				countImages--;
    			console.log(error);
    			outJson[saveIndex].medal_num = imgName
    			if( countImages == 0) {
  					completeSaveMedal(outJson)
  				}
  			});
		} else {
    			outJson[j].medal_num = imgName
    		if(countImages == 0 && j == names.length -1 ) {
  				completeSaveMedal(outJson)
  			}
		}


	}
}

function completeSaveMedal(outJson) {
	axios.post('/php/save_medals.php',outJson)
  	.then(function (response) {
  		if(response.data == "ok") {
  			alert("Успешное сохранение")
  		} else {
  			alert("Ошибка сохранения: "+response.data)
  		}
  	});
}
function addBestPunkt(el) {
	var parent = el.parentNode

	var newEl = document.createElement("div")
	newEl.setAttribute('class','input-group mb-3')

	newEl.innerHTML = '\ <input value="" type="text" class="form-control best-list" aria-describedby="button-addon2">\
  <div class="input-group-append">\
    <button class="btn btn-outline-secondary" onclick="deleteBestPunkt(this)" type="button" >Удалить</button>\
  </div>'

  	parent.insertBefore(newEl,parent.children[parent.children.length-2])

}

</script>



		<div class="card content-main">
        <h5 class="card-header">Настройки</h5>
        <div class="card-body">

HTML;

echo '<div class="form-group">
    <label for="exampleInputEmail1">Ссылка на youtube</label>
    <input value="'.$config['youtube'].'" type="text" class="form-control" id="youtube-link" aria-describedby="emailHelp">
  </div>';
echo '<div class="form-group">
    <label for="exampleInputEmail1">E-mail для принятия сообщений об обратном звонке</label>
    <input value="'.$config['email'].'" type="email" class="form-control" id="email-change" aria-describedby="emailHelp">
  </div>';

echo '<div class="form-group">
    <label for="exampleInputEmail1">Логин для входа</label>
    <input value="'.$config['login'].'" type="email" class="form-control" id="login-change" aria-describedby="emailHelp">
  </div>';
  echo '<div class="form-group">
    <label for="exampleInputEmail1">Пароль для входа</label>
    <input value="'.$config['password'].'" type="email" class="form-control" id="password-change" aria-describedby="emailHelp">
  </div>';
 echo '<div class="form-group">
    <label for="exampleInputEmail1">Номер медали на главной 1:</label>
    <input value="'.$linkMedalMainPage[0].'" type="email" class="form-control" id="main-page-medal-1" aria-describedby="emailHelp">
  </div>';
  echo '<div class="form-group">
    <label for="exampleInputEmail1">Номер медали на главной 2:</label>
    <input value="'.$linkMedalMainPage[1].'" type="email" class="form-control" id="main-page-medal-2" aria-describedby="emailHelp">
  </div>';

echo '<button type="button" onclick="saveSettings()" class="btn btn-primary">Сохранить</button>';



        echo '</div></div>';


echo <<<HTML


<div class="card content-main">
        <h5 class="card-header">Ссылки для контактов</h5>
        <div class="card-body">
HTML;

foreach ($socialList as $key => $value) {
	  echo '<div class="form-group">';
	echo '    <label>'.$value->name.'</label>';
	echo '    <input value="'.$value->link.'" type="text" class="form-control social-contact"></div>';
}

echo '<button type="button" onclick="saveSocialLinks(this)" class="btn btn-primary">Сохранить</button>';


 echo '      </div></div>';



echo '<div class="card content-main">
        <h5 class="card-header">Таблица с ценами</h5>
        <div class="card-body">';

echo '    <label>Строки таблицы. Разделение между значениями – ";". Начинается с "|" – выделиить жирным, с "$" – ключ с локализации.</label>';
foreach ($tablePrices as $key => $value) {
	  echo '<div class="form-group">';
	echo '    <input value="'.$value.'" type="text" class="form-control table-price"></div>';
}


echo '<button type="button" onclick="savePriceTable(this)" class="btn btn-primary">Сохранить</button>';

echo '</div></div>';


echo '	<div class="card content-main">
        <h5 class="card-header">Список значков</h5>
        <div class="card-body">';




foreach ($medalList as $value) {


echo '<div class="card" style="width: 100%;">
  <img src="/img/MedalList/'.$value->medal_num.'" class="card-img-top">

  <div class="card-body">';

  echo '<div class="form-group">
    <label for="exampleFormControlFile1">Картинка</label>
    <input type="file" class="form-control-file" accept="image/*" id="exampleFormControlFile1">
      <input class="img-name" value="'.$value->medal_num.'" type="hidden">

  </div>';
  echo '    <label>Название</label>';
echo '    <input value="'.$value->desc.'" type="text" class="form-control medal-name" aria-describedby="emailHelp">';

  echo '    <label>Характеристики</label>';

foreach ($value->list_best as $best) {

	echo '<div class="input-group mb-3">
  <input value="'.$best.'" type="text" class="form-control best-list" aria-describedby="button-addon2">
  <div class="input-group-append">
    <button class="btn btn-outline-secondary" onclick="deleteBestPunkt(this)" type="button" >Удалить</button>
  </div>
</div>';
}
echo '<button type="button" onclick="addBestPunkt(this)" class="btn btn-success">Добавить</button>';
echo '<button type="button" onclick="deleteBestPunkt(this)" class="btn btn-danger">Удалить значок</button>';

echo  '</div></div>';

}

echo '<button type="button" onclick="addMedal(this)" class="btn btn-success">Добавить значок</button>';

echo '<button type="button" onclick="saveListMedal(this)" class="btn btn-primary">Сохранить список значков</button>';

echo '</div></div>';


	} else {
		setcookie('admin_session', '', time()-3600, '/php');
		echo '<script type="text/javascript">
			alert("Неправильный логин или пароль!")
			document.location.reload(true);

			</script>';
	}
}




?>









<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>

